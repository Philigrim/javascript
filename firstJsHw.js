/*
    1. Write two binary functions, add and mul, that take two numbers and return their sum and product. 

*/

function add(a, b) {
    return a + b;
  }
  
  function mul(a, b) {
    return a * b;
  }
  
  add(3, 4);    //  7  
  mul(3, 4);    // 12
  
  
  
  /*
      2. Write a function that takes an argument and returns a function that returns that argument.  
  */
  
  function identify(value){
    return () => {
      return value;
    }
  }
  
  const idf = identify(3);
  idf();    // 3
  
  
  
  /*
      3. Write a function addf that adds from two invocations.
  */
  
  
  function addf(a){
    return (b) => {
      return a + b;
    }
  }    
  
  addf(3)(4);    // 7
  
  
  
  /*
      4. Write a function that takes a binary function, and makes it callable with two invocations.
  */
  
  
  function applyf(functionName){
    return (a) => {
      return (b) => {
        return functionName(a, b);
      };
    };
  }
  
  const addff = applyf(add);                // (addff is addf in example)
  addff(3)(4);           // 7               // (addff is addf in example)
  applyf(mul)(5)(6);    // 30
  
  /*
      5. Write a function that takes a function and an argument, and returns a function that can supply a second argument.  
  */
  
  function curry(functionName, a) {
    return (b) => {
      return functionName(a, b);
    };
  }
  
  const add3 = curry(add, 3);  
  add3(4);             // 7  
  curry(mul, 5)(6);    // 30  
  
  
  
  /*
      6. Write a function twice that takes a binary function and returns a unary function that passes its argument to the binary function twice.
  */
  
  function twice(functionName){
    return (a) => {
      return functionName(a, a);
    };
  }
  
  const double = twice(add);  
  double(11);    // 22  
  const square = twice(mul);  
  square(11);    // 121
  
  
  
  /*
      7. Write a function composeu that takes two unary functions and returns a unary function that calls them both. 
  */
  
  function composeu(functionOne, functionTwo){
    return (a) => {
      return functionTwo(functionOne(a));
    };
  }
  
  composeu(double, square)(3);    // 36
  
  
  
  /*
      8. Write a function that adds from many invocations, until it sees an empty invocation.
  */
  
  function addg(a) {
    function addNext(b) {
      if (b === undefined ) {
        return a;
      }
      a += b;
      return addNext;
    }
    if(a !== undefined ){
      return addNext;
    }
    else {
      return 0;
    }
  }
  
  addg(3)(4)(5)();     // 12 
  addg(1)(2)(4)(8)();  // 15
  
  
  
  /*
      9. Write a function that will take a binary function and apply it to many invocations.
  */
  
  function applyg(functionName){
    return (a) => {
      function takeNextNumber(b){
        if(b === undefined){
          return a;
        }
        a = functionName(a, b);
        return takeNextNumber;
      }
      if(a !== undefined ){
        return takeNextNumber;
      }
      else {
        return 0;
      }
    }
  }
  
  applyg(add)(3)(4)(5)();       // 12 
  applyg(mul)(1)(2)(4)(8)();    // 64
  
  /*
      10. Make a function that returns a function that will return the next fibonacci number.
  */
  
  let fibonaccif = function() {
    let a = 0;
    let b = 0;
    return function() {
      if(a == 0){
        b++;
      }
      var x = a;
      a = b;
      b += x;
      return x;
    }
  }
  
  const fib = fibonaccif();  
  fib();    // 0  
  fib();    // 1 
  fib();    // 1
  fib();    // 2
  fib();    // 3
  fib();    // 5